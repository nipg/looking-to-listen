import sys

sys.path.append('./model/model/')
import AV_model as AV
from option import ModelMGPU, latest_file
from tensorflow.keras.callbacks import ModelCheckpoint, LearningRateScheduler, Callback
from tensorflow.keras.models import Model, load_model
from data_load import AVGenerator
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras import optimizers
import os
from loss import audio_discriminate_original as audio_loss_original
from loss import audio_discriminate_loss2 as audio_loss
import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.keras import backend as K

K.clear_session()


# Resume Model
resume_state = False

# Parameters
# input // n_gpus > 0
frequency = 500
people_num = 2
epochs = 1
initial_epoch = 0
gamma_loss = 0.1
beta_loss = gamma_loss * 2

# Accelerate Training Process
workers = 12
MultiProcess = False
NUM_GPU = 4
# input batch is the global size and will be roughly split amongst the workers, 28 to 30 works
batch_size = 36
val_steps = 1600 // batch_size

# PATH
model_path = './saved_AV_models'  # model path
chkpt_path = './tf_ckpts'
database_path = 'data/'
log_path = './log_AV'

# create folder to save models
folder = os.path.exists(model_path)
if not folder:
    os.makedirs(model_path)
    print('create folder to save models')


folder = os.path.exists(log_path)
if not folder:
    os.makedirs(log_path)
    print('create folder to save logs')

filepath = model_path + "/AVmodel-newloss-" + str(people_num) + "p.h5"
checkpoint = tf.keras.callbacks.ModelCheckpoint(filepath, verbose=1, save_freq='epoch', save_weights_only=True)

# format: mix.npy single.npy single.npy
trainfile = []
valfile = []

with open((database_path + 'AVdataset_train_3_single.txt'), 'r') as t:
    trainfile = t.readlines()
with open((database_path + 'AVdataset_val_3_single.txt'), 'r') as v:
    valfile = v.readlines()

# the training steps
if resume_state:
    if NUM_GPU > 1:
        mirrored_strategy = tf.distribute.MirroredStrategy()
        latest_file = latest_file(model_path + '/')
        with mirrored_strategy.scope():
            parallel_model = AV.AV_model(people_num)
            parallel_model.load_weights(latest_file)
            loss = audio_loss_original(people_num=people_num)
            updated_lr = 0.000003 * (36.0 / 28.0)
            adam = optimizers.Adam(learning_rate=updated_lr)
            parallel_model.compile(loss=loss, optimizer=adam)
        #K.set_value(parallel_model.optimizer.learning_rate, 0.0000003)

    else:
        latest_file = latest_file(model_path + '/')
        single_model = AV.AV_model(people_num)
        single_model.load_weights(latest_file)
        loss = audio_loss_original(people_num=people_num)
        updated_lr = 0.000003 * (36.0 / 28.0)
        adam = optimizers.Adam(learning_rate=updated_lr)
        single_model.compile(loss=loss, optimizer=adam)
        # K.set_value(AV_model.optimizer.learning_rate, 0.00003)
else:
    if NUM_GPU > 1:

        strategy = tf.distribute.MirroredStrategy()
        with strategy.scope():
            parallel_model = AV.AV_model(people_num)
            updated_lr = 0.000003 * (36.0 / 28.0)
            adam = optimizers.Adam(learning_rate=updated_lr)
            loss = audio_loss_original(people_num=people_num)
            parallel_model.compile(loss=loss, optimizer=adam)
    else:
        single_model = AV.AV_model(people_num)
        adam = optimizers.Adam(learning_rate=0.00000375)
        loss = audio_loss_original(batch_size=batch_size, people_num=people_num)
        # loss = audio_loss(gamma=gamma_loss, beta=beta_loss, people_num=people_num)
        single_model.compile(optimizer=adam, loss=loss)

train_generator = AVGenerator(trainfile, database_path=database_path, batch_size=batch_size, shuffle=True)
val_generator = AVGenerator(valfile, database_path=database_path, batch_size=batch_size, shuffle=True)

if NUM_GPU > 1:

    parallel_model.fit(train_generator,
                       validation_data=val_generator,
                       epochs=1,
                       callbacks=[TensorBoard(log_dir='./log_AV', update_freq=3, histogram_freq=1), checkpoint])  # batch_sized changed to 16


if NUM_GPU <= 1:
    history=single_model.fit_generator.fit(train_generator,
                       validation_data=val_generator,
                       epochs=1,
                       callbacks=[TensorBoard(log_dir='./log_AV', update_freq=3, histogram_freq=1), checkpoint])  # batch_sized changed to 16



