import sys
sys.path.append('../utils')
import utils
import pandas as pd
import os


def make_audio(location, name, d_csv, start_idx, end_idx):
    for i in range(start_idx,end_idx):
        f_name = name + str(i)
        link = "https://www.youtube.com/watch?v="+d_csv.loc[i][0]
        start_time = d_csv.loc[i][1]
        end_time = start_time+3.0
        utils.download(location,f_name,link)
        utils.cut(location,f_name,start_time,end_time)
        print("\r Process audio... ".format(i) + str(i), end="")
    print("\r Finish !!", end="")


def process_audio(process_range, loc='../videos/dev/mp4/'):
    # loc = '../videos/vox2_dev_mp4_partah~/dev/mp4/'
    i = 0
    start_counter = 0
    sr = 16000
    for folder in sorted(os.listdir(loc)):
        temp_0 = loc + folder
        for folder_1 in sorted(os.listdir(temp_0)):
            temp_1 = temp_0 + '/' + folder_1
            for file in sorted(os.listdir(temp_1)):
                if file[0]=='o':
                    print('Skipping file: ', file)
                    #command = 'cd %s;' % temp_1
                    #command += 'rm %s;' % file
                    #os.system(command)
                    continue

                start_time = 0.0
                command = 'cd %s;' % temp_1
                command += 'ffprobe -loglevel error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ' + file
                var = float(os.popen(command).read())

                num_possible_segments = var // 3
                if num_possible_segments < 1:
                    continue
                else:
                    start_counter += num_possible_segments

                if start_counter <= process_range[0]:
                    continue

                for _ in range(int(num_possible_segments)):
                    f_name = str(i)
                    command = 'cd %s;' % temp_1
                    command += 'ffmpeg -loglevel error -i ' + file + ' -f wav -ar %d -ac 1 -vn ../../../../../audio_data/audio_train/%s.wav;' % (sr, f_name)
                    os.system(command)

                    length = 3.0
                    audio_loc = 'audio_train'
                    command = 'cd %s;' % audio_loc
                    command += 'sox -q %s.wav trim_%s.wav trim %s %s;' % (f_name, f_name, start_time, length)
                    command += 'rm %s.wav;' % f_name
                    os.system(command)

                    print('\r Created sample number ' + str(i), end="")
                    start_time += 3.0
                    i += 1
                    if (i + process_range[0]) > process_range[1]:
                        return

utils.mkdir('audio_train')
process_range = (0,500000)
process_audio(process_range)


