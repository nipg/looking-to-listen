from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import sys
import os
import datetime
sys.path.append("../utils")
import utils
import pandas as pd
import time


def video_download(loc,d_csv,start_idx,end_idx):
    # Only download the video from the link
    # loc        | the location for downloaded file
    # v_name     | the name for the video file
    # cat        | the catalog with audio link and time
    # start_idx  | the starting index of the video to download
    # end_idx    | the ending index of the video to download

    for i in range(start_idx,end_idx):
        command = 'cd %s;' % loc
        f_name = str(i)
        link = "https://www.youtube.com/watch?v="+d_csv.loc[i][0]
        start_time = d_csv.loc[i][1]
        end_time = start_time + 3.0
        start_time = datetime.timedelta(seconds=start_time)
        end_time = datetime.timedelta(seconds=end_time)
        command += 'ffmpeg -i $(youtube-dl -f ”mp4“ --get-url ' + link + ') ' + '-c:v h264 -c:a copy -ss %s -to %s %s.mp4' \
                % (start_time, end_time, f_name)
        #command += 'ffmpeg -i %s.mp4 -r 25 %s.mp4;' % (f_name,'clip_' + f_name) #convert fps to 25
        #command += 'rm %s.mp4' % f_name
        os.system(command)

def generate_frames(loc,start_idx,end_idx):
    # get frames for each video clip
    # loc        | the location of video clip
    # v_name     | v_name = 'clip_video_train'
    # start_idx  | the starting index of the training sample
    # end_idx    | the ending index of the training sample

    utils.mkdir('frames')
    for i in range(start_idx, end_idx):
        command = 'cd %s;' % loc
        f_name = str(i)
        command += 'ffmpeg -i %s.mp4 -y -f image2  -vframes 75 ../frames/%s-%%02d.jpg' % (f_name, f_name)
        os.system(command)


def download_video_frames(loc,d_csv,start_idx,end_idx,rm_video):
    # Download each video and convert to frames immediately, can choose to remove video file
    # loc        | the location for downloaded file
    # cat        | the catalog with audio link and time
    # start_idx  | the starting index of the video to download
    # end_idx    | the ending index of the video to download
    # rm_video   | boolean value for delete video and only keep the frames

    utils.mkdir('frames')
    for i in range(start_idx, end_idx):
        command = 'cd %s;' % loc
        f_name = str(i)
        link = "https://www.youtube.com/watch?v="+d_csv.loc[i][0]
        start_time = d_csv.loc[i][1]
        #start_time = 90
        start_time = time.strftime("%H:%M:%S.0",time.gmtime(start_time))
        command += 'youtube-dl --prefer-ffmpeg -f "mp4" -o o' + f_name + '.mp4 ' + link + ';'
        command += 'ffmpeg -i o'+f_name+'.mp4'+' -ss '+str(start_time)+' -t '+"3 "+f_name+'.mp4;'
        command += 'rm o%s.mp4;' % f_name
        #ommand += 'ffmpeg -i %s.mp4 -r 25 %s.mp4;' % (f_name, 'clip_' + f_name)  # convert fps to 25
        #command += 'rm %s.mp4;' % f_name

        #converts to frames
        #command += 'ffmpeg -i %s.mp4 -y -f image2  -vframes 75 ../frames/%s-%%02d.jpg;' % (f_name, f_name)
        command += 'ffmpeg -i %s.mp4 -vf fps=25 ../frames/%s-%%02d.jpg;' % (f_name, f_name)
        #command += 'ffmpeg -i %s.mp4 ../frames/%sfr_%%02d.jpg;' % ('clip_' + f_name, f_name)

        if rm_video:
            command += 'rm %s.mp4;' % f_name
        os.system(command)
        print("\r Process video... ".format(i) + str(i), end="")
    print("\r Finish !!", end="")

def process_videos(process_range, loc='../videos/dev/mp4/'):
    # loc = '../videos/vox2_dev_mp4_partah~/dev/mp4/'
    utils.mkdir('frames')
    start_counter = 0
    i = 0
    for folder in sorted(os.listdir(loc)):
        temp_0 = loc + folder
        for folder_1 in sorted(os.listdir(temp_0)):
            temp_1 = temp_0 + '/' + folder_1
            for file in sorted(os.listdir(temp_1)):
                if file[0] == 'o':
                    print('Skipping file: ', file)
                    #command = 'cd %s;' % temp_1
                    #command += 'rm %s;' % file
                    #os.system(command)
                    continue

                start_time = 0
                command = 'cd %s;' % temp_1
                command += 'ffprobe -loglevel error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ' + file

                # var = os.popen(command).read()
                # print('The cl output is: ', var)
                # var = float(var)

                var = float(os.popen(command).read())

                num_possible_segments = var // 3
                if num_possible_segments < 1:
                    continue
                else:
                    start_counter += num_possible_segments

                if start_counter <= process_range[0]:
                    continue

                for _ in range(int(num_possible_segments)):
                    f_name = str(i)
                    command = 'cd %s;' % temp_1
                    command += 'ffmpeg -loglevel error -i ' + file + ' -ss ' + str(start_time) + ' -t ' + "3 o" + f_name + '.mp4;'
                    command += 'ffmpeg -loglevel error -i o%s.mp4 -vf fps=25 ../../../../../video_data/frames/%s-%%02d.jpg;' % (f_name, f_name)
                    command += 'rm o%s.mp4;' % f_name
                    os.system(command)
                    print('\r Created sample number ' + str(i), end="")
                    start_time += 3
                    i += 1
                    if (i + process_range[0]) > process_range[1]:
                        with open('../data_length.txt', 'a') as f:
                            f.write(str(process_range[1] - process_range[0]))
                        return
    # with open('../data_length.txt', 'a') as f:
    #     f.write(str(process_range[1] - process_range[0] + 1))





utils.mkdir('video_train')

process_range = (0, 500000)
process_videos(process_range)
