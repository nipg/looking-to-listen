import sys
sys.path.append ('./ model / model')
sys.path.append ('./ model / utils')
from tensorflow.keras.models import load_model
# from option import ModelMGPU
import os
import scipy.io.wavfile as wavfile
import numpy as np
# import utils
import tensorflow as tf



#parameters
people = 2
num_gpu=1
persons = ['A', 'B']

#path
model_path = './saved_AV_model/AVmodel-2p-099.h5'
result_path = './predict/'
os.makedirs(result_path,exist_ok=True)

# database = './data/AV_model_database/mix/'
database = './inference/AV_model_database/single/'
face_emb = ['./model/face_embedding/face1022_emb/A/', './model/face_embedding/face1022_emb/B/']
print('Initialing Parameters......')

#loading data
print('Loading data ......')
test_file = []
with open('./inference/AVdataset_val.txt','r') as f:
    test_file = f.readlines()


def get_data_name(line,people=people,database=database,face_emb=face_emb):
    persons = ['A', 'B']
    parts = line.split() # get each name of file for one testset
    # mix_str = parts[0]
    single_str = parts[0]
    # name_list = mix_str.replace('.npy','')
    name = single_str.replace('.npy','')
    name = name.replace('single-','',1)
    single_idxs = persons
    file_path = database + single_str
    audio = np.load(file_path)
    face_embs = np.zeros((1,25,1,1792,people))
    for i, person in enumerate(persons):
        a = np.load(face_emb[i] +"%05d_face_emb_"%int(name) + person + ".npy")
        print(a.shape)
        face_embs[0,:,:,:,i] = np.load(face_emb[i] +"%05d_face_emb_"%int(name) + person + ".npy")

    return audio,single_idxs,face_embs

#result predict
# av_model = load_model(model_path,custom_objects={'tf':tf})
if num_gpu>1:
    parallel = ModelMGPU(av_model,num_gpu)
    for line in test_file:
        mix,single_idxs,face_emb = get_data_name(line,people,database,face_emb)
        mix_ex = np.expand_dims(mix,axis=0)
        cRMs = parallel.predict([mix_ex,face_emb])
        cRMs = cRMs[0]
        prefix =''
        for idx in single_idxs:
            prefix +=idx+'-'
        for i in range(len(cRMs)):
            cRM =cRMs[:,:,:,i]
            assert cRM.shape ==(298,257,2)
            F = utils.fast_icRM(mix,cRM)
            T = utils.fase_istft(F,power=False)
            filename = result_path+str(single_idxs[i])+'.wav'
            wavfile.write(filename,16000,T)

if num_gpu<=1:
    for line in test_file:
        mix,single_idxs,face_emb = get_data_name(line,people,database,face_emb)
        mix_ex = np.expand_dims(mix,axis=0)
        cRMs = av_model.predict([mix_ex,face_emb])
        cRMs = cRMs[0]
        prefix =''
        for idx in single_idxs:
            prefix +=idx+'-'
        for i in range(len(cRMs)):
            cRM =cRMs[:,:,:,i]
            assert cRM.shape ==(298,257,2)
            F = utils.fast_icRM(mix,cRM)
            T = utils.fase_istft(F,power=False)
            filename = result_path+persons[i]+'.wav'
            wavfile.write(filename,16000,T)
