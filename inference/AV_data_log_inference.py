with open('./AV_model_database/dataset.txt', 'r') as tr:
    lines = tr.readlines()
    for line in lines:
        info = line.strip().split('.')
        num1 = info[0].strip().split('-')[1]
        new_line = line.strip() + ' ' + num1 + '_face_emb_A.npy' + ' ' + num1 + '_face_emb_B.npy\n'
        with open('AVdataset_val.txt', 'a') as f:
            f.write(new_line)