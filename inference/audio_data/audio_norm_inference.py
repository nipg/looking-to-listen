import librosa
import os
import numpy as np
import scipy.io.wavfile as wavfile
import math

audio_range = (0, 19)
loc = 'audio_source'
name = 'audio_inf'
interval_length = 1.0

command = 'cd %s & ' %  loc
command += 'ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 %s.wav' % (name)
total_length = float(os.popen(command).read())

num_segments = math.floor(math.ceil(total_length) / interval_length)
num_segments = int(num_segments)

if not os.path.exists('./norm_audio_test'):
    os.mkdir('./norm_audio_test')

for idx in range(num_segments):
    print('Processing audio %s'%idx)
    path = './audio_source/trim_%s.wav' % idx
    norm = './norm_audio_test/trim_audio_test%s.wav' % idx
    if os.path.exists(path):
        audio, _ = librosa.load(path, sr=16000)
        max = np.max(np.abs(audio))
        # norm_audio = np.power(audio, 0.3)
        norm_audio = np.divide(audio, max)
        wavfile.write(norm,16000,norm_audio)


