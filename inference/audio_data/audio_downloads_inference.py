import os
import math


loc = 'audio_source'
interval_length = 1.0
sr = 16000
name = 'audio_inf'

if not os.path.exists('./audio_source'):
    os.mkdir('./audio_source')


command = 'cd %s & ' % loc
command += 'ffmpeg -i speakerA.mp4 -ar %d -ac 1 -f wav %s.wav & ' % (sr, name)
# command += 'del speakerA.mp4'
os.system(command)

command = 'cd %s & ' % loc
command += 'ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 %s.wav' % (name)
total_length = float(os.popen(command).read())

num_segments = math.floor(math.ceil(total_length) / interval_length)
num_segments = int(num_segments)
print("The number of segments are: ", num_segments)
print("The number of frames in a segment are: ", interval_length * 25)


start_time = 0
command = 'cd %s & ' % loc
for i in range(num_segments):
    end_time = start_time + interval_length
    command += 'sox %s.wav trim_%s.wav trim %s %s & ' % (name,i,start_time,interval_length)
    start_time = end_time
os.system(command)

command = 'cd %s & ' % loc
command += 'del audio_inf.wav'
command += 'del speakerA.mp4'
os.system(command)


