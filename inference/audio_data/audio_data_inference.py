import sys

sys.path.append('../../model/utils')
import os
import librosa
import numpy as np
import utils


data_range = (0, 4)  # data usage to generate database
audio_norm_path = os.path.expanduser("./norm_audio_test")
database_path = '../AV_model_database'
num_speakers = 2
max_generate_data = 50

# initial data dir
def init_dir(path=database_path):
    if not os.path.exists(path):
        os.mkdir(path)

    if not os.path.isdir('%s/single' % path):
        os.mkdir('%s/single' % path)

# Generate datasets dir list
def generate_data_list(data_r=data_range, audio_norm_pth=audio_norm_path):
    audio_path_list = []

    for idx in range(data_r[0], data_r[1]):
        path = audio_norm_pth + '/trim_audio_test%d.wav' % idx
        if os.path.exists(path):
            audio_path_list.append((idx, path))

    print('\nlength of the path list: ', len(audio_path_list))
    return audio_path_list


# audio generate stft data(numpy)
def audio_to_numpy(audio_path_list, data_path=database_path, fix_sr=16000):
    for idx, path in audio_path_list:
        print('\r audio numpy generating... %d' % ((idx / len(audio_path_list)) * 100), end='')
        data, _ = librosa.load(path, sr=fix_sr)
        # data = utils.fast_stft(data)
        data = utils.fast_stft(data, power=True)
        name = 'single-%05d' % idx
        with open('%s/dataset.txt' % data_path, 'a') as f:
            f.write('%s.npy' % name)
            f.write('\n')
        np.save(('%s/single/%s.npy' % (data_path, name)), data)
    print()


if __name__ == '__main__':
    init_dir()
    audio_path_list = generate_data_list()
    print(audio_path_list)
    audio_to_numpy(audio_path_list)
