from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import sys
import os
import datetime
sys.path.append("../utils")
import utils
import pandas as pd
import time

vid_range = (0, 3)
interval_length = 1.0

def get_video_frames(loc,start_idx,end_idx,rm_video, person):
    # Download each video and convert to frames immediately, can choose to remove video file
    # loc        | the location for downloaded file
    # cat        | the catalog with audio link and time
    # start_idx  | the starting index of the video to download
    # end_idx    | the ending index of the video to download
    # rm_video   | boolean value for delete video and only keep the frames

    # utils.mkdir('frames')
    for i in range(start_idx, end_idx):
        utils.mkdir(person + '/frames')
        person_loc = person + '/' + loc
        command = 'cd %s & ' % person_loc
        f_name = str(i)
        command += 'ffmpeg -i speaker%s.mp4 -ss %d -t %d %s.mp4 & ' % (person, i * interval_length, interval_length, f_name)
        command += 'ffmpeg -i %s.mp4 -vf fps=25 ../frames/%s-%%02d.jpg & ' % (f_name, f_name)

        if rm_video:
            command += 'del %s.mp4;' % f_name
        os.system(command)
        print("\r Process video... ".format(i) + str(i), end="")
    print("\r Finish !!", end="")


# download video , convert to images separately
#avh.video_download(loc='video_train',v_name='video_train',cat=cat_train,start_idx=2,end_idx=4)
#avh.generate_frames(loc='video_train',v_name='clip_video_train',start_idx=2,end_idx=4)

# download each video and convert to frames immediately
if not os.path.exists('./A'):
    os.mkdir('./A')

if not os.path.exists('./B'):
    os.mkdir('./B')

persons =['A', 'B']
for person in persons:
    get_video_frames(loc='video_train', start_idx=vid_range[0], end_idx=vid_range[1], rm_video=False, person=person)