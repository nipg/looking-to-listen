# create custom loss function for training
import sys
sys.path.append('./model/utils/')
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Lambda, multiply, add
import tensorflow as tf
import utils

def audio_discriminate_loss(gamma=0.1,people_num=2):
    def loss_func(S_true,S_pred,gamma=gamma,people_num=people_num):
        sum = 0
        for i in range(people_num):
            sum += K.sum(K.flatten((K.square(S_true[:,:,:,i]-S_pred[:,:,:,i]))))
            for j in range(people_num):
                if i != j:
                    sum -= gamma*K.sum(K.flatten((K.square(S_true[:,:,:,i]-S_pred[:,:,:,j]))))

        loss = sum / (people_num*298*257*2)
        return loss
    return loss_func


def audio_discriminate_loss2(gamma=0.1,beta = 2*0.1,people_num=2):
    def loss_func(S_true,S_pred,gamma=gamma,beta=beta,people_num=people_num):
        sum_mtr = K.zeros_like(S_true[:,:,:,:,0])
        for i in range(people_num):
            sum_mtr += K.square(S_true[:,:,:,:,i]-S_pred[:,:,:,:,i])
            for j in range(people_num):
                if i != j:
                    sum_mtr -= gamma*(K.square(S_true[:,:,:,:,i]-S_pred[:,:,:,:,j]))

        for i in range(people_num):
            for j in range(i+1,people_num):
                #sum_mtr -= beta*K.square(S_pred[:,:,:,i]-S_pred[:,:,:,j])
                #sum_mtr += beta*K.square(S_true[:,:,:,:,i]-S_true[:,:,:,:,j])
                pass
        #sum = K.sum(K.maximum(K.flatten(sum_mtr),0))

        loss = K.mean(K.flatten(sum_mtr))

        return loss
    return loss_func


def fast_icRM(Y, crm, K_=10, C=0.1):
    M = crm

    S1 = tf.multiply(M[:, :, :, 0], Y[:, :, :, 0]) - tf.multiply(M[:, :, :, 1], Y[:, :, :, 1])

    S2 = tf.multiply(M[:, :, :, 0], Y[:, :, :, 1]) - tf.multiply(M[:, :, :, 1], Y[:, :, :, 0])

    return tf.concat([tf.expand_dims(S1, 3), tf.expand_dims(S2, 3)], axis=3)



def audio_discriminate_original(people_num=2, batch_size=2):
    def loss_func(S_true, S_pred, people_num=people_num, batch_size=batch_size):
        # sum_mtr = K.zeros_like(S_true[0,:,:,:,0])
        sum_mtr = tf.zeros_like(S_true[0,:,:,:,0])

        for j in range(people_num):
            pred = fast_icRM(S_true[:,:,:,:,-1], S_pred[:,:,:,:,j])
            sum_mtr += tf.math.square(S_true[:,:,:,:,j]-pred)
            # sum_mtr += K.square(S_true[:,:,:,:,j]-pred)

        # loss = K.sum(K.flatten(sum_mtr))
        loss = tf.math.reduce_mean(tf.reshape(sum_mtr, [-1]))

        return loss
    return loss_func





